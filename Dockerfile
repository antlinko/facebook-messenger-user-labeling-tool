FROM python:3.5

WORKDIR /facebook-lookup
COPY . /facebook-lookup

RUN apt-get update && pip install --upgrade pip && pip install -r requirements.txt

COPY docker /facebook-lookup/docker

RUN chmod -R 777 /facebook-lookup/docker/start.sh

CMD ["/facebook-lookup/docker/start.sh"]
EXPOSE 8000
