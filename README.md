# Facebook Messenger Labeling Tool

Helper for Facebook Messenger API.
Script associate broadcast labels with PSID's from CSV and JSON files.

# Installation

## Configure Docker environment

```
cp docker/environments.example docker/environments
```

Configure page access token in docker/environments
```
FB_GRAPH_API_ACCESS_TOKEN=<your-page-access-token>
```

## Run containers

```
docker-compose up
```

# Usage
## Send params via URL

Make request to the API. Default endpoint is http://127.0.0.1. 
Send label name in GET parameter 'label' and comma-separated list of PSID's in GET parameter psids.
```
http://127.0.0.1/api/?label=test_label_1&psids=12345,67890
```

## Upload JSON file
Upload JSON file with label name and list of PSID's. You can use file test_json_file.json as example
```
http://127.0.0.1/api/json
```

## Upload CSV file
Upload CSV file with list of PSID's without header. Send label name in GET parameter 'label'. You can use file test_csv_file.csv as example
```
http://127.0.0.1/api/csv?label=test_label_1
```

## Response

API response contains list of PSID's and results of the label adding.

```json
    {
        "status":"ok",
        "error":{},
        "data":{
            "12345":{
                "status":"ok",
                "error":{}
            },
            "67890":{
                "status":"fail",
                "error":{
                    "message":"(#100) The user ID is not valid.",
                    "type":"OAuthException",
                    "code":100,
                    "fbtrace_id":"HqZD1t9xZsx"
                }
            }
        }
    }
```

