from django.shortcuts import render

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from .mixins import URLParametersValidationMixin, FileValidationMixin


class FacebookLookupView(APIView, URLParametersValidationMixin):
    permission_classes = (AllowAny,)

    def get(self, request, **kwargs):
        self.validate_request(request)
        if self.failed_response is not None:
            return self.failed_response
        return Response(
            data=self.success_response
        )


class JSONFacebookLookupView(APIView, FileValidationMixin):
    permission_classes = (AllowAny,)
    is_json = True

    def get(self, request, **kwargs):
        context = {
            'title': 'Upload JSON file',
            'head': 'Upload JSON file'
        }
        return render(request, 'form.html', context)

    def post(self, request, **kwargs):
        self.validate_request(request)
        if self.failed_response:
            return self.failed_response
        return Response(
            data=self.success_response
        )


class CSVFacebookLookupView(APIView, FileValidationMixin):
    permission_classes = (AllowAny,)
    is_csv = True

    def get(self, request, **kwargs):
        context = {
            'title': 'Upload CSV file',
            'head': 'Upload CSV file'
        }
        return render(request, 'form.html', context)

    def post(self, request, **kwargs):
        self.validate_request(request)
        if self.failed_response:
            return self.failed_response
        return Response(
            data=self.success_response
        )


